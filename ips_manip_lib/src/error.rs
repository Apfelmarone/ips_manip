/// Crate-specific error type.
#[derive(thiserror::Error, Debug)]
#[non_exhaustive]
pub enum Error {
    #[error("I/O")]
    Io {
        #[from]
        source: std::io::Error,
    },
    #[error("ips")]
    Ips {
        #[from]
        source: ips::Error,
    },
    #[error("Invalid input: type: {ty}, limit = {size_limit} bytes, actual = {actual_size} bytes")]
    InvalidInput {
        ty: String,
        size_limit: usize,
        actual_size: usize,
    },
}

/// Crate-specific result type.
pub type Result<T> = core::result::Result<T, Error>;
