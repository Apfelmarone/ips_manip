use crate::{Error, Patch, Result};

mod simple;

/// The algorithm used when computing a patch file.
///
/// The algorithm can affect the size of a patch file and the elapsed time,
/// but it is guaranteed that any algorithm will give a correct patch file.
#[derive(Debug, Clone, Copy, Eq, PartialEq)]
pub enum DiffAlgorithm {
    /// Simple greedy algorithm.
    Simple,
    /// Tries to find the smallest possible patch file.
    Smallest,
    /// Tries to find the smallest possible patch, but allows overwriting to unchanged bytes.
    SmallestWithOrigContent,
}

/// Computes a patch from two binary data.
pub fn find_patch(orig: &[u8], modified: &[u8], algorithm: DiffAlgorithm) -> Result<Patch> {
    const LIMIT: usize = 1 << 24;
    if orig.len() > LIMIT {
        return Err(Error::InvalidInput {
            ty: "orig".to_owned(),
            size_limit: LIMIT,
            actual_size: orig.len(),
        });
    }
    if modified.len() > LIMIT {
        return Err(Error::InvalidInput {
            ty: "modified".to_owned(),
            size_limit: LIMIT,
            actual_size: modified.len(),
        });
    }
    Ok(match algorithm {
        DiffAlgorithm::Simple => self::simple::find_patch(orig, modified),
        DiffAlgorithm::Smallest => todo!(),
        DiffAlgorithm::SmallestWithOrigContent => todo!(),
    })
}
