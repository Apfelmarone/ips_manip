use crate::{Hunk, Patch};

pub fn find_patch(orig: &[u8], modified: &[u8]) -> Patch {
    let origsize = orig.len();
    let modifiedsize = modified.len();
    let mut hunks = vec![];
    let mut thishunk = vec![];
    let mut thisoffset = 0;
    for i in 0..modifiedsize {
        if i >= origsize || orig[i] != modified[i] {
            thishunk.push(modified[i]);
        } else {
            if !thishunk.is_empty() {
                hunks.push(Hunk::Regular {
                    offset: thisoffset as u32,
                    payload: std::mem::take(&mut thishunk),
                })
            }
            thisoffset = i + 1;
        }
    }
    if !thishunk.is_empty() {
        hunks.push(Hunk::Regular {
            offset: thisoffset as u32,
            payload: std::mem::take(&mut thishunk),
        })
    }
    let mut truncation = None;
    if origsize > modifiedsize {
        truncation = Some(modifiedsize as u32);
    }
    Patch { hunks, truncation }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn find_patch_test_1() {
        let orig = b"\x01\x02\x03";
        let modified = b"\x04\x02\x06";
        // 01 -> 04, 03 -> 06,
        let patch = find_patch(orig, modified);
        let hunks = &patch.hunks;
        assert_eq!(hunks.len(), 2);
        assert_eq!(
            hunks[0],
            Hunk::Regular {
                offset: 0,
                payload: vec![0x04],
            }
        );
        assert_eq!(
            hunks[1],
            Hunk::Regular {
                offset: 2,
                payload: vec![0x06],
            }
        );
        assert_eq!(patch.truncation, None);
    }
}
