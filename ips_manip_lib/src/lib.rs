use byteorder::{BigEndian, WriteBytesExt};
use std::io::Write;

/// Applies a patch to a byte array.
mod apply;
/// Creates a patch file from a given pair of byte arrays.
pub mod diff;
/// Error handling.
pub mod error;
pub use crate::error::{Error, Result};

/// Represents an IPS patch file.
#[derive(Debug, Clone)]
pub struct Patch {
    pub hunks: Vec<Hunk>,
    pub truncation: Option<u32>,
}

/// <http://fileformats.archiveteam.org/wiki/IPS_(binary_patch_format)>
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Hunk {
    Regular {
        offset: u32, // 24-bit
        payload: Vec<u8>,
    },
    RLE {
        offset: u32, // 24-bit
        length: u16,
        byte: u8,
    },
}

impl Patch {
    /// Converts this `Patch` instance to a byte array.
    pub fn to_bytes(&self) -> Result<Vec<u8>> {
        let hunks = &self.hunks;
        let mut result = vec![];
        result.write_all(b"PATCH")?;
        for hunk in hunks {
            match *hunk {
                Hunk::Regular {
                    offset,
                    ref payload,
                } => {
                    result.write_u24::<BigEndian>(offset as u32)?;
                    result.write_u16::<BigEndian>(payload.len() as u16)?;
                    result.write_all(payload)?;
                }
                Hunk::RLE {
                    offset,
                    length,
                    byte,
                } => {
                    result.write_u24::<BigEndian>(offset as u32)?;
                    result.write_u16::<BigEndian>(0x0000)?;
                    result.write_u16::<BigEndian>(length)?;
                    result.write_u8(byte)?;
                }
            }
        }
        result.write_all(b"EOF")?;
        if let Some(truncation) = self.truncation {
            result.write_u24::<BigEndian>(truncation as u32)?;
        }
        Ok(result)
    }
    /// Converts a byte array to a `Patch` instance.
    pub fn from_bytes(input: &[u8]) -> Result<Self> {
        let patch = ips::Patch::parse(input)?;
        let mut hunks = vec![];
        for hunk in patch.hunks() {
            let offset = hunk.offset() as u32;
            let payload = hunk.payload();
            if payload.len() >= 4 && payload.iter().all(|&x| x == payload[0]) {
                hunks.push(Hunk::RLE {
                    offset,
                    length: payload.len() as u16,
                    byte: payload[0],
                });
            } else {
                hunks.push(Hunk::Regular {
                    offset,
                    payload: payload.to_vec(),
                })
            }
        }
        Ok(Self {
            hunks,
            truncation: patch.truncation().map(|x| x as u32),
        })
    }
    /// Applies a patch to a byte array.
    /// Returns an error if orig.len() > 1 << 24.
    pub fn apply(&self, orig: &[u8]) -> Result<Vec<u8>> {
        crate::apply::apply(orig, self)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn to_bytes_is_inverse_of_from_bytes_nop() {
        let data: &[u8] = b"PATCHEOF";
        let patch = Patch::from_bytes(data).unwrap();
        let result = patch.to_bytes().unwrap();
        assert_eq!(data, result);
    }

    #[test]
    fn to_bytes_is_inverse_of_from_bytes_simple() {
        let data: &[u8] = b"PATCH\x00\x00\x00\x00\x02ABEOF"; // offset = 0, length = 2, payload = b"AB"
        let patch = Patch::from_bytes(data).unwrap();
        let result = patch.to_bytes().unwrap();
        assert_eq!(data, result);
    }

    #[test]
    fn to_bytes_is_inverse_of_from_bytes_rle() {
        let data: &[u8] = b"PATCH\x00\x00\x00\x00\x00\x01\x23AEOF"; // offset = 0, length = 0x123, byte = b"A"
        let patch = Patch::from_bytes(data).unwrap();
        let result = patch.to_bytes().unwrap();
        assert_eq!(data, result);
    }
}
