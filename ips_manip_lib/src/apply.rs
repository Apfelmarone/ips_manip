use crate::{Error, Hunk, Patch, Result};

pub fn apply(orig: &[u8], patch: &Patch) -> Result<Vec<u8>> {
    const LIMIT: usize = 1 << 24;
    if orig.len() > LIMIT {
        return Err(Error::InvalidInput {
            ty: "orig".to_owned(),
            size_limit: LIMIT,
            actual_size: orig.len(),
        });
    }
    let mut result = orig.to_vec();
    for hunk in &patch.hunks {
        match *hunk {
            Hunk::Regular {
                offset,
                ref payload,
            } => {
                if result.len() < offset as usize + payload.len() {
                    result.resize(offset as usize + payload.len(), 0);
                }
                for i in 0..payload.len() {
                    result[offset as usize + i] = payload[i];
                }
            }
            Hunk::RLE {
                offset,
                length,
                byte,
            } => {
                if result.len() < offset as usize + length as usize {
                    result.resize(offset as usize + length as usize, 0);
                }
                for i in 0..length as usize {
                    result[offset as usize + i] = byte;
                }
            }
        }
    }
    if let Some(truncation) = patch.truncation {
        result.truncate(truncation as usize);
    }
    Ok(result)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn apply_works_1() {
        let orig = b"ABC";
        let patch = Patch {
            hunks: vec![
                Hunk::Regular {
                    offset: 0,
                    payload: vec![b'D'],
                },
                Hunk::Regular {
                    offset: 2,
                    payload: vec![b'F'],
                },
            ],
            truncation: None,
        };
        let result = apply(orig, &patch).unwrap();
        assert_eq!(result, b"DBF");
    }
    #[test]
    fn apply_works_2() {
        let orig = b"ABCDEFG";
        let patch = Patch {
            hunks: vec![
                // [1, 10] becomes 'Z'.
                Hunk::RLE {
                    offset: 1,
                    length: 10,
                    byte: b'Z',
                },
            ],
            truncation: None,
        };
        let result = apply(orig, &patch).unwrap();
        assert_eq!(result, b"AZZZZZZZZZZ");
    }
    #[test]
    fn apply_works_3() {
        let orig = b"ABCDEFG";
        let patch = Patch {
            hunks: vec![Hunk::Regular {
                offset: 0,
                payload: b"XYZ".to_vec(),
            }],
            truncation: Some(5),
        };
        let result = apply(orig, &patch).unwrap();
        assert_eq!(result, b"XYZDE");
    }
}
